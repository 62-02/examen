package com.example.examenmovil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private EditText editTextNombre, editTextBase, editTextAltura;
    private Button buttonLimpiar, buttonSiguiente, buttonSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextNombre = findViewById(R.id.editTextNombre);
        editTextBase = findViewById(R.id.editTextBase);
        editTextAltura = findViewById(R.id.editTextAltura);
        buttonLimpiar = findViewById(R.id.buttonLimpiar);
        buttonSiguiente = findViewById(R.id.buttonSiguiente);
        buttonSalir = findViewById(R.id.buttonSalir);

        buttonSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validarCampos()) {
                    Intent intent = new Intent(MainActivity.this, RectanguloActivity.class);
                    intent.putExtra("nombre", editTextNombre.getText().toString());
                    intent.putExtra("base", Float.parseFloat(editTextBase.getText().toString()));
                    intent.putExtra("altura", Float.parseFloat(editTextAltura.getText().toString()));
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, "Por favor, rellena todos los campos", Toast.LENGTH_SHORT).show();
                }
            }
        });

        buttonLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        buttonSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private boolean validarCampos() {
        return !editTextNombre.getText().toString().isEmpty() &&
                !editTextBase.getText().toString().isEmpty() &&
                !editTextAltura.getText().toString().isEmpty();
    }

    private void limpiarCampos() {
        editTextNombre.setText("");
        editTextBase.setText("");
        editTextAltura.setText("");
    }
}
