package com.example.examenmovil;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class RectanguloActivity extends AppCompatActivity {
    private EditText editTextBase, editTextAltura, editTextNombre;
    private RadioButton radioButtonArea, radioButtonPerimetro;
    private TextView textViewResultado;
    private Button buttonCalcular, buttonRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo);

        editTextBase = findViewById(R.id.editTextBase);
        editTextAltura = findViewById(R.id.editTextAltura);
        editTextNombre = findViewById(R.id.editTextNombre);
        radioButtonArea = findViewById(R.id.radioButtonArea);
        radioButtonPerimetro = findViewById(R.id.radioButtonPerimetro);
        textViewResultado = findViewById(R.id.textViewResultado);
        buttonCalcular = findViewById(R.id.buttonCalcular);
        buttonRegresar = findViewById(R.id.buttonRegresar);

        // Obtén los datos pasados desde MainActivity
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            editTextNombre.setText(extras.getString("nombre"));
            editTextBase.setText(String.valueOf(extras.getFloat("base")));
            editTextAltura.setText(String.valueOf(extras.getFloat("altura")));
        }

        // Desactivar la edición de los EditText
        editTextNombre.setEnabled(false);
        editTextBase.setEnabled(false);
        editTextAltura.setEnabled(false);

        buttonCalcular.setOnClickListener(v -> calcular());

        buttonRegresar.setOnClickListener(v -> finish());
    }

    private void calcular() {
        float base = Float.parseFloat(editTextBase.getText().toString());
        float altura = Float.parseFloat(editTextAltura.getText().toString());

        Rectangulo rectangulo = new Rectangulo(base, altura);

        if (radioButtonArea.isChecked()) {
            textViewResultado.setText("Área: " + rectangulo.calcularArea());
        } else if (radioButtonPerimetro.isChecked()) {
            textViewResultado.setText("Perímetro: " + rectangulo.calcularPerimetro());
        }
    }
}
